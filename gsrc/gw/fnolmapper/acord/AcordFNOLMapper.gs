package gw.fnolmapper.acord

uses gw.api.fnolmapper.FNOLMapper
uses gw.api.fnolmapper.FNOLMapperException
uses java.util.Date

uses xsd.acord.ACORD_Type
uses xsd.acord.ACORD_Type_ACORDREQ
uses xsd.acord.ClaimsNotificationAddRq_Type

uses gw.xml.xsd.types.XSDTime
uses gw.api.address.AddressJurisdictionHandler
uses gw.blp.enhancements.carglass.util.CarglassUtil_Ext
uses gw.plugin.contact.ab800.ABContactSystemPlugin
uses gw.plugin.contact.search.ContactSearchFilter
uses java.lang.Exception

/**
 * Main class for mapping an ACORD XML document to an FNOL Claim. This is a reference
 * implementation only: not all ACORD properties or codes are mapped. Specific additional requirements
 * may require extension or modification of this and related classes. Type list mappings are configurable
 * using the standard TypecodeMapping mechanism with the "acord" namespace.
 * PRE: requires (and only processes) a <ClaimsNotificationAddRq> child element in the XML document
 */
@Export
class AcordFNOLMapper implements FNOLMapper 
{   
  var config:AcordConfig
  var mapperFactory:IMapperFactory
  var contactMapper:IContactMapper
  var contactManager:ContactManager
  
  construct() {
    config = new AcordConfig()
    mapperFactory = config.MapperFactory
    contactManager = new ContactManager()
    contactMapper = mapperFactory.getContactMapper()
  }
  
  /**
   * Populates Claim data from ACORD XML.
   */
  override function populateClaim(claim:Claim, acordXML:String) {
    var acordRequest = ACORD_Type.parse(acordXML).ACORDREQ
    //get first "Add Request" for Claim
    var addRequest = this.getFirstValidRequest(acordRequest)
    if(addRequest==null)
      throw new FNOLMapperException("No Claims Notification Add Request (<ClaimsNotificationAddRq>) in document")
    config.getLogger().info("Processing Claim Service Request UID=" + addRequest.RqUID!=null ? addRequest.RqUID : "Not Given")
    populatePolicy(claim.Policy, addRequest)
    populateClaimInfo(claim, addRequest.ClaimsOccurrence)
    populatePrincipals(claim, addRequest)
    populateClaimParties(claim, addRequest)
    populateLossDetails(claim, addRequest)
    //BLP: Carglass starts- fetch Policy details from PC; if found then set policy object in claim.Policy else populatePolicy
    if(addRequest.getAttributeValue("FNOLcaller").equalsIgnoreCase("Carglass")){
      var verifiedPolicy = getPolicyFromPC_Ext(claim.Policy, addRequest)
      if(null != verifiedPolicy){
        claim.Policy = verifiedPolicy
        claim.Policy.Verified = true
      }else{
        populatePolicy(claim.Policy, addRequest)
      }
    } else{

      populatePolicy(claim.Policy, addRequest)
    }
//BLP: Carglass ends
    populateIncidentsAndExposures(claim, addRequest)
    if (addRequest.getAttributeValue("FNOLcaller").equalsIgnoreCase("Carglass")){
      claim.ClaimSource = typekey.ClaimSource.TC_CARGLASS
      claim.addNote(addRequest.getAttributeValue("debtorBalance").toString().trim())
      claim.CoverageInQuestion = false
      claim.IncidentReport = false
      populateVendorCompany_Ext(claim, addRequest.getAttributeValue("contactTaxID"))
    }
  }
//BLP: Carglass starts
  private function populateVendorCompany_Ext(claim: Claim, taxID: String) {
    try {
      var searchCriteria = new ContactSearchCriteria()
      searchCriteria.TaxID = taxID
      searchCriteria.ContactSubtype = typekey.Contact.TC_COMPANYVENDOR
      var contactSearchResult = new ABContactSystemPlugin().searchContacts(searchCriteria, new ContactSearchFilter())
      var abContact = contactSearchResult.getContacts().first()
      var claimContact = new ClaimContact()
      claimContact.Contact = abContact
      var claimContactRole = new ClaimContactRole()
      claimContactRole.Role = typekey.ContactRole.TC_CHECKPAYEE
      claimContact.addToRoles(claimContactRole)
      claim.addToContacts(claimContact)
    } catch (e: Exception) {
      print("Exception in  populateVendorCompany_Ext == " + e.StackTraceAsString)
      throw new FNOLMapperException(e.StackTraceAsString)
    }
  }

  private function getPolicyFromPC_Ext(claimPolicy: Policy, req: ClaimsNotificationAddRq_Type):Policy{
    var policy : Policy
    try{
      claimPolicy.Claim.LossDate = req.ClaimsOccurrence.LossDt
      claimPolicy.PolicyNumber =  req.Policy.PolicyNumber
      var retrievedResults =  new gw.plugin.pcintegration.pc800.PolicySearchPCPlugin().retrievePolicyFromPolicy(claimPolicy)
     policy =  retrievedResults.Result
    } catch(e:Exception){
      print("Exception in  getPolicyFromPC_Ext == " + e.StackTraceAsString)
      throw new FNOLMapperException(e.StackTraceAsString)
    }  return policy
  }
//BLP: Carglass ends
  //Finds the first valid Claim Add Request or returns null
  private function getFirstValidRequest(acordReq:ACORD_Type_ACORDREQ): ClaimsNotificationAddRq_Type {
    var addRequest:ClaimsNotificationAddRq_Type = null
    for(claimSvcReq in acordReq.ClaimsSvcRqs) {
      if(addRequest!=null) break;
      for(msg in claimSvcReq.CLAIMSSVCRQMSGSs) {
          if(msg.Choice.ClaimsNotificationAddRq!=null) {
            addRequest = msg.Choice.ClaimsNotificationAddRq
            break;
          }
      }
    }
    return addRequest
  }
  
  /**
   * Sets basic info on the claim.
   */
  private function populateClaimInfo(claim:Claim, claimInfo:xsd.acord.ClaimsOccurrence_Type) {
    //set loss date if present
    if(claimInfo.LossDt_elem!=null) {
      if(claimInfo.LossTime!=null)
        claim.LossDate = getDateTime(claimInfo.LossDt_elem, claimInfo.LossTime)
      else
        claim.LossDate = claimInfo.LossDt_elem.toDate()
    }
    if(claimInfo.Addr!=null)
      claim.LossLocation = mapperFactory.getAddressMapper().getAddress(claimInfo.Addr)
  }
    
  //populates Claim policy information
  private function populatePolicy(claimPolicy:Policy, req:ClaimsNotificationAddRq_Type) {
      mapperFactory.getPolicyMapper().populate(claimPolicy, req)
  }
  
  //populates Insured or Principal contacts as Claim contacts
  private function populatePrincipals(claim:Claim, req:ClaimsNotificationAddRq_Type) {
      for(contact in req.InsuredOrPrincipals) {
          var claimContact = contactMapper.getContact(contact, claim.Policy)
          if(claimContact!=null) {
            contactManager.addContact(contact.id, claimContact)
            claim.addToContacts(claimContact)
          }
      }
  }
  
  //populates the Claim parties as Claim contacts
  private function populateClaimParties(claim:Claim, claimReq:ClaimsNotificationAddRq_Type) {
      for(claimParty in claimReq.ClaimsPartys) {
        var claimContact = contactMapper.getContact(claimParty)
        if(claimContact!=null) {
//BLP: Carglass starts
        var insureRole = claimContact.Roles.firstWhere(\elt1 -> elt1.Role == ContactRole.TC_INSURED)
        if (insureRole != null){
          insureRole.Policy = claim.Policy
        }
	//BLP: Carglass ends
          contactManager.addContact(claimParty.id, claimContact)
          claim.addToContacts(claimContact)
//BLP: Carglass starts
        if(claimParty.hasRole("reptBy") ){
          claim.ReportedByType = config.getPersonRelationTypeMap().get(claimParty.ClaimsPartyInfo.RelationshipToInsuredCd_elem.Text)
        }
        if(claimParty.hasRole("MCO") ){
          claim.MainContactType  =config.getPersonRelationTypeMap().get(claimParty.ClaimsPartyInfo.RelationshipToInsuredCd_elem.Text)
        }
//BLP: Carglass ends
      }
    }
  }
  
  //populates the Loss details on the Claim
  private function populateLossDetails(claim: Claim, claimReq:ClaimsNotificationAddRq_Type) {
      var claimOccur =  claimReq.ClaimsOccurrence
      claim.LossType = config.getLossTypeMap().get(claimReq.Policy.LOBCd_elem.Text)
      claim.LossDate = getDateTime(claimOccur.LossDt_elem, claimOccur.LossTime)
      var address = claimReq.ClaimsOccurrence.Addr==null ? new Address() : 
        mapperFactory.getAddressMapper().getAddress(claimReq.ClaimsOccurrence.Addr)
      claim.LossLocation = address
      var country = address.Country
      claim.JurisdictionState = AddressJurisdictionHandler.getJurisdiction(address)
      claim.LOBCode = config.getLOBTypeMap().get(claimReq.Policy.LOBCd_elem.Text)
      claim.ReportedDate = claimOccur.ClaimsReporteds[0].ReportedDt_elem.toDate()
      claim.Description = claimOccur.IncidentDesc
    if (claimReq.getAttributeValue("FNOLcaller").equalsIgnoreCase("Carglass")){
      //srishti
      claim.FaultRating = typekey.FaultRating.TC_NOFAULT
      claim.HowReported = typekey.HowReportedType.TC_WALKIN
      claim.LossCause = typekey.LossCause.TC_GLASSBREAKAGE
      if (null != claimReq.ClaimsPartys[1].GeneralPartyInfo.NameInfos[0].Choice.CommlName.CommercialName) {                  //TODO
        claim.ReportedByType = typekey.PersonRelationType.TC_EMPLOYEE
      } else {
        claim.ReportedByType = typekey.PersonRelationType.TC_CLAIMANT
      }
    }
  }
  
  //populates the incidents and exposures on the Claim
  private function populateIncidentsAndExposures(claim: Claim, claimReq:ClaimsNotificationAddRq_Type) {
      var exposureMapper = mapperFactory.getExposureMapper(contactManager)
      var incidentMapper = mapperFactory.getIncidentMapper(contactManager)
      //Choice of one of the following...
      //auto loss
      for(autoLoss in claimReq.Choice.AutoLossInfos) {
        var incident = incidentMapper.getVehicleIncident(claim, autoLoss)
      if (claimReq.getAttributeValue("FNOLcaller").equalsIgnoreCase("Carglass")){
        //srishti
        incident.LossOccured = typekey.LossOccured.TC_OTHER
        incident.DriverRelation = typekey.PersonRelationType.TC_SELF
      }
        exposureMapper.getVehicleExposure(claim, autoLoss, incident)
      }
      //general liability loss
      for(liabilityLoss in claimReq.Choice.LiabilityLossInfos) {
        var incident = incidentMapper.getGenLiabilityIncident(claim, liabilityLoss)
        exposureMapper.getGenLiabilityExposure(claim, liabilityLoss, incident)
      }
      //property loss
      for(propertyLoss in claimReq.Choice.PropertyLossInfos) {
        var incident = incidentMapper.getPropertyIncident(claim, propertyLoss)
        exposureMapper.getPropertyExposure(claim, propertyLoss, incident)
      }
      //worker's comp loss
      for(wcLoss in claimReq.Choice.WorkCompLossInfos) {
        if(wcLoss.EmployeeInfo!=null)
          addEmploymentData(claim, wcLoss.EmployeeInfo)
        var incident = incidentMapper.getWorkCompIncident(claim, wcLoss)
        exposureMapper.getWorkCompExposure(claim, wcLoss, incident)
      }

      //create incidents and exposures for claimant's ClaimsInjuredInfo, if present
      for(claimParty in claimReq.ClaimsPartys) {
        if(claimParty.hasRole(AcordUtil.ROLE_CLAIMANT)) {
          if(claimParty.ClaimsInjuredInfo!=null) {
            var incident = incidentMapper.getInjuryIncident(claim, claimParty.ClaimsInjuredInfo)
            exposureMapper.getInjuryExposure(claim, claimParty.ClaimsInjuredInfo, incident)
          }
        }
      }
  }
  
  /**
   * Additional data for Worker's Comp claims
   */
  private function addEmploymentData(claim:Claim, employeeInfo:xsd.acord.EmployeeInfo_Type) {
    var employmentData = new EmploymentData()
    employmentData.HireDate = employeeInfo.HiredDt_elem.toDate()
    employmentData.HireState = State.get(employeeInfo.HiredStateProvCd);
    employmentData.EmploymentStatus = config.getEmploymentStatusTypeMap().get(employeeInfo.EmploymentStatusCd_elem.Text)
    employmentData.DepartmentCode = employeeInfo.RegularDept
    if(employeeInfo.EmployeePays.HasElements) {
      var pay = employeeInfo.EmployeePays.first()
      employmentData.WageAmount = AcordUtil.getCurrencyAmount(pay.AvgAmt.Amt,pay.AvgAmt.CurCd)
    }
    employmentData.WagePaymentCont = employeeInfo.SalaryContinuanceInd
    employmentData.PaidFull = employeeInfo.FullPayDayInjuredInd
    if(employeeInfo.EmployeeSchedules!=null) {
      var schedule = employeeInfo.EmployeeSchedules.first()
      employmentData.NumDaysWorked = schedule.NumDaysPerWeek
      employmentData.NumHoursWorked = schedule.NumHoursPerDay
    }
    claim.InjuredOnPremises = employeeInfo.OccurredPremisesCd_elem.Text.equalsIgnoreCase("Employer")
    claim.SafetyEquipProv = employeeInfo.SafeguardsProvidedInd
    claim.SafetyEquipUsed = employeeInfo.SafeguardsUsedInd
    claim.EmploymentData = employmentData
  }
  
  //returns a java.util.Date that reflects the appropriate Date & Time
  private function getDateTime(dateElem:xsd.acord.Date, xsdTime:XSDTime) : Date {
    return dateElem.toDateTime(xsdTime)
  }
}
