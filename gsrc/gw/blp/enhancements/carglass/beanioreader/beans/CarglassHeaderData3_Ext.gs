package gw.blp.enhancements.carglass.beanioreader.beans

uses java.util.Date



/**
 * Created with IntelliJ IDEA.
 * User: Srishti.Malik1
 * Date: 21/4/16
 * Time: 5:17 PM
 * To change this template use File | Settings | File Templates.
 */
class CarglassHeaderData3_Ext {
  var recordType : String as RecordType
  var correlationID: String as CorrelationID
  var invoiceReportedDate: Date as InvoiceReportedDate
   var invoiceExpirationDate: Date as InvoiceExpirationDate
   var invoiceDaysExpired: String as InvoiceDaysExpired
  var carglassReference: String as CarglassReference
  var vehicleLicensePlate: String as VehicleLicensePlate
  var policyNumber: String as PolicyNumber
  var claimRefInsurer: String as ClaimRefInsurer
  var lossDate: Date as LossDate
  var creditForInvoiceNumber : String as CreditForInvoiceNumber
  var invoiceOrCreditNote : String as InvoiceOrCreditNote
  var vin: String as Vin
  var damageCauseCode : String as DamageCauseCode
  var damageCauseDescription : String as DamageCauseDescription
  var vehicleMake: String as VehicleMake
  var vehicleModel: String as VehicleModel
  var vehicleType: String as VehicleType
  var vehicleManufacturingPeriod: String as VehicleManufacturingPeriod
  var vehicleYear: String as VehicleYear
  var vehicleMileage: String as VehicleMileage
}