package gw.blp.enhancements.carglass.beanioreader.beans
/**
 * Created with IntelliJ IDEA.
 * User: Srishti.Malik1
 * Date: 21/4/16
 * Time: 5:17 PM
 * To change this template use File | Settings | File Templates.
 */
class CarglassHeaderData0_Ext {
  var recordType : String as RecordType
  var correlationID : String as CorrelationID
  var repairShopNumber : String as RepairShopNumber
  var repairShopName : String as RepairShopName
  var repairShopAddress1 : String as RepairShopAddress1
  var repairShopAddress2  : String as RepairShopAddress2
  var repairShopAddressPostalCode: String as RepairShopAddressPostalCode
  var repairShopAddressCity  : String as RepairShopAddressCity
  var repairShopTelephone    : String as RepairShopTelephone
  var repairShopFax        : String as RepairShopFax
  var repairShopBusinessLicense       : String as RepairShopBusinessLicense
}