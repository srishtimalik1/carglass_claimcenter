package gw.blp.enhancements.carglass.beanioreader.beans
/**
 * Created with IntelliJ IDEA.
 * User: Srishti.Malik1
 * Date: 21/4/16
 * Time: 5:17 PM
 * To change this template use File | Settings | File Templates.
 */
class CarglassHeaderData2_Ext {
  var recordType : String as RecordType
  var correlationID : String as CorrelationID
  var insuredName : String as InsuredName
  var insuredAddress : String as InsuredAddress
  var insuredPostalCode  : String as InsuredPostalCode
  var insuredAddressCity: String as InsuredAddressCity
  var vatLiable  : String as VatLiable
  var vatNumber  : String as VatNumber
  var vatRecovery    : String as VatRecovery
  var insuredLanguage : String as InsuredLanguage
  var insuredTelephone        : String as InsuredTelephone
}