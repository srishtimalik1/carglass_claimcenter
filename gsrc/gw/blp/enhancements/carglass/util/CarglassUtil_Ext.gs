package gw.blp.enhancements.carglass.util

uses gw.blp.enhancements.carglass.beanioreader.beans.CarglassDataBean_Ext
uses gw.blp.enhancements.carglass.acordmapper.CarglassBulkInvoiceMapper_Ext
uses gw.blp.enhancements.carglass.acordmapper.CarglassClaimMapper_Ext
uses gw.pl.persistence.core.Bundle
uses gw.plugin.contact.ab800.ABContactSystemPlugin
uses gw.plugin.contact.search.ContactSearchFilter
uses gw.webservice.cc.cc801.claim.ClaimAPI
uses gw.webservice.cc.cc801.financials.bulkpay.BulkInvoiceAPI
uses org.beanio.StreamFactory
uses org.slf4j.LoggerFactory

uses java.io.File
uses java.lang.Exception
uses java.lang.StringBuilder
uses java.util.ArrayList
uses java.util.Date
uses java.util.HashMap
uses java.util.Map
uses java.text.SimpleDateFormat
uses gw.blp.enhancements.carglass.constants.CarglassConstants_Ext
uses java.lang.NullPointerException


/**
 * Main class to read the source file received from Carglass which is used to create Claims.
 * Autho: Srishti.Malik1
 * Date: 5/5/16
 *
 */
public class CarglassUtil_Ext {
	private static var _logger = LoggerFactory.getLogger("carglassDailyFileLog")
	private static var _contactTaxID: String as ContactTaxID
  /**
   * function importFile()- is the main method which read the file content, validate the data, process the data and then create claims and bulk invoice.
   * @Param eachFile File received from Carglass which is to be processed
   */
  public function importFile(eachFile: File) {
		try {
    var validationMap = new  HashMap<String, Object>()
      var file = CarglassValidator_Ext.isFileProcessed(eachFile.Name)
      if(null != file){
          _logger.error("File ${eachFile.Name} is already processed on ${file.CreateTime}")
      } else{
			var factory = StreamFactory.newInstance()
			//factory.loadResource("blp/enhancements/carglass/beanioreader/mappingxml/CarglassRecordsMapping_Ext.xml");
			factory.load("C:\\Work\\Accelerator\\GWStudio\\CC_v802\\modules\\configuration\\gsrc\\gw\\blp\\enhancements\\carglass\\beanioreader\\mappingxml\\CarglassRecordsMapping_Ext.xml")
			var inputFile = factory.createReader(CarglassConstants_Ext.RECORD_STREAM, eachFile);
			var record = inputFile.read()
			while (record != null) {
				if (inputFile.RecordName.equalsIgnoreCase("f0")) {
					var fileData = record as Map
					var hqTaxID = (fileData.get(CarglassConstants_Ext.HQ_TAX_ID) as StringBuilder).substring(CarglassConstants_Ext.INT_TWO) as StringBuilder
					hqTaxID = hqTaxID.insert(CarglassConstants_Ext.INT_THREE, CarglassConstants_Ext.HYPHEN)
					ContactTaxID = hqTaxID.insert(CarglassConstants_Ext.INT_SIX, CarglassConstants_Ext.HYPHEN)  as String
					_logger.debug("Carglass HeadQuarter Tax ID: " + ContactTaxID)
				} else if (inputFile.RecordName.equalsIgnoreCase(CarglassConstants_Ext.RECORD)){
					var claimBean = record as CarglassDataBean_Ext
					 validationMap = CarglassValidator_Ext.isDataValid(record)
					var errorMessage = validationMap.get(CarglassConstants_Ext.ERROR_MESSAGE_KEY)
					if (null != errorMessage) {
						errorMessage = errorMessage.toString()
					} else {
						errorMessage = null
					}

					var claimDataEntity = CarglassValidator_Ext.CGReferenceExist(claimBean.H3.Vin,claimBean.H3.LossDate, claimBean.H3.CarglassReference, eachFile.Name)
					if (null == claimDataEntity) {
						gw.transaction.Transaction.runWithNewBundle(\transactionBundle -> {
							transactionBundle.add(claimDataEntity)
							var newClaim = populateClaimDataStaging(eachFile.Name, errorMessage, record, transactionBundle)
							var invoiceEntity = populateInvoiceDataStaging(claimBean.H5.get(CarglassConstants_Ext.DEBTOR_BALANCE).toString().trim() as String, claimBean.H3.CorrelationID, transactionBundle)
							newClaim.addToCarglassInvoices(invoiceEntity)
							claimDataEntity = newClaim
							if (validationMap.get(CarglassConstants_Ext.IS_PROCESS)){
								var claim = createClaim(claimBean, claimDataEntity, ContactTaxID)
								claim.ClaimSource = typekey.ClaimSource.TC_CARGLASS
							}
						}, "su")
					} else {
						gw.transaction.Transaction.runWithNewBundle(\transactionBundle -> {
							transactionBundle.add(claimDataEntity)
							var invoiceEntity = populateInvoiceDataStaging(claimBean.H5.get(CarglassConstants_Ext.DEBTOR_BALANCE).toString().trim() as String, claimBean.H3.CorrelationID, transactionBundle)
							claimDataEntity.addToCarglassInvoices(invoiceEntity)
						}, "su")
					}
				}
				record = inputFile.read()
			}
    //  if (validationMap.get(CarglassConstants_Ext.IS_PROCESS)){
			createBulkInvoice(eachFile.Name)
			updateClaimStatus()
    //    }
    }
		} catch (e: Exception) {
			_logger.error("Exception in importFile() " + e.StackTraceAsString)
			print("importFile() -- " + e.StackTraceAsString)
		}

	}
  /**
   * function populateClaimDataStaging()- Inserts flat file data into CarglassDataImportStaging table.
   * @Param fileName Name of the received file.
   * @Param errorMessage Contains the error messages
   * @Param record Contains the source file data received from Carglass
   * @Param transactionBundle Transaction object
   * @return CarglassDataImportStaging  entity object
   */
	private function populateClaimDataStaging(fileName: String, errorMessage: String, record: Object, transactionBundle: Bundle): CarglassDataImportStaging {
		var claimEntity = transactionBundle.add(new CarglassDataImportStaging())
		try {
			var claimBean = record as CarglassDataBean_Ext
			claimEntity.CarglassReferenceNumber = claimBean.H3.CarglassReference
			claimEntity.CorrelationID = claimBean.H0.CorrelationID
			claimEntity.PolicyNumber = claimBean.H3.PolicyNumber
			claimEntity.LossDate = claimBean.H3.LossDate.toSQLDate()
			claimEntity.VIN = claimBean.H3.Vin
			claimEntity.ReportedDate = claimBean.H3.InvoiceReportedDate
			claimEntity.VATLiable = claimBean.H2.VatLiable
			claimEntity.Language = claimBean.H2.InsuredLanguage
			claimEntity.InvoiceOrCreditNote = claimBean.H3.InvoiceOrCreditNote
			claimEntity.FileName = fileName
			claimEntity.ErrorMessage = errorMessage
			if (null != errorMessage) {
				claimEntity.Status = "Error"
				}  else{
					claimEntity.Status = typekey.ClaimState.TC_DRAFT      as String
			}
		} catch (e: Exception) {
			_logger.error("Exception in populateClaimDataStaging() " + e.StackTraceAsString)
		}
		return claimEntity
	}
  /**
   * function populateInvoiceDataStaging()- Inserts flat file data into CarglassInvoiceStaging table.
   * @Param amount Invoice Amount received in source file
   * @Param correlationID Unique id received in source file for each claim data
   * @Param transactionBundle Transaction object
   * @return CarglassInvoiceStaging  entity object
   */
	private function populateInvoiceDataStaging(amount: String, correlationID: String, transactionBundle: Bundle): CarglassInvoiceStaging {
		var invoiceEntity = transactionBundle.add(new CarglassInvoiceStaging())
		try {
			invoiceEntity.CorrelationID = correlationID
			invoiceEntity.Amount = amount.trim()
		} catch (e: Exception) {
			_logger.error("Exception in populateInvoiceDataStaging() " + e.StackTraceAsString)
		}
		return invoiceEntity
	}

  /**
   * function createClaim()- Prepares acordXML object using source file data
   * Create a new claim by sending populated acordXML object to ClaimAPI.
   * @Param CarglassDataBean_Ext flat file data object
   * @Param CarglassDataImportStaging claim staging entity object
   * @Param taxID Carglass headquarter tax ID to be used for searching contact details from Contact Manager
   * @return Claim  entity object
   */
	private function createClaim(claimBean: CarglassDataBean_Ext, claimEntity: CarglassDataImportStaging, taxID: String): Claim {
		var claim: Claim

		try {
			var cgAcordXML = new CarglassClaimMapper_Ext(claimBean, taxID).AcordXML
			print(cgAcordXML)
			var claimPublicID = new ClaimAPI().importAcordClaimFromXML(cgAcordXML)
			claim = getClaimEntity(claimPublicID)
			claimEntity.ClaimNumber = claim
			//check if policy is verified or not
			print( "is policy verified --  " +claim.Policy.Verified)
       var infoMessage = "Verified Policy"
		 if(!claim.Policy.Verified)  {
     claimEntity.Status = CarglassConstants_Ext.INVALID_POLICY
      infoMessage =     "Unverified Policy"
     }
      print("For Carglass Reference Number ${claimBean.H3.CarglassReference}, new claim ${claim.ClaimNumber} has been created.")
      _logger.info("For Carglass Reference Number: ${claimBean.H3.CarglassReference}, new claim: ${claim.ClaimNumber} has been created on ${infoMessage}: ${claimBean.H3.PolicyNumber}")
      _logger.info("For Carglass Reference Number: ${claimBean.H3.CarglassReference}, H5 data in source file: Total Amount is: ${claimBean.H5.get("totalAmount").toString().trim()}, Insured Balance is: ${claimBean.H5.get("insuredBalance").toString().trim()}, Debtor Balance is: ${claimBean.H5.get(CarglassConstants_Ext.DEBTOR_BALANCE).toString().trim()} ")
		} catch (e: Exception) {
			_logger.error("Exception in createClaim()  " + e.StackTraceAsString)
		}
		return claim
	}
  /**
   * function getClaimEntity()- Fetches the Claim entity object for the newly created Claim
   * @Param claimPublicID Newly created Claim's public ID
   * @return Claim  entity object
   */
	private function getClaimEntity(claimPublicID: String): Claim {
		var claim: Claim
		try {
			claim = gw.api.database.Query.make(Claim).compare(Claim#PublicID, Equals, claimPublicID).select().FirstResult
		} catch (e: Exception) {            ;
			_logger.error("Exception in getClaimEntity() " + e.StackTraceAsString)
		}
		return claim
	}
  /**
   * function createBulkInvoice()- Populates Bulk Invoice DTO
   * Creates Bulk Invoice for all the verified claims.
   * After Bulk invoice creation, submits the invoice for approval.
   * @Param fileName  Name of the received file.
   */
	private function createBulkInvoice(fileName: String) {
		try {
			var bulkInvoiceDTO = new CarglassBulkInvoiceMapper_Ext().prepareBulkInvoiceDTO(ContactTaxID, fileName)
			if (bulkInvoiceDTO.NewInvoiceItems.Count != 0){
				gw.transaction.Transaction.runWithNewBundle(\bundle -> {
					var bulkInvoice = new BulkInvoiceAPI().createBulkInvoice(bulkInvoiceDTO)
					_logger.debug("Bulk Invoice public id: ${bulkInvoice}")
					var invoice = gw.api.database.Query.make(entity.BulkInvoice).compare(BulkInvoice#PublicID, Equals, bulkInvoice).select().first()
					invoice = bundle.add(invoice)
					invoice.validate()
					if (invoice.Valid) {
						invoice.submitForApproval()
						invoice.requestInvoice()
					}
				}, "su")
			}
		} catch (e: Exception) {
			_logger.error("Exception in createBulkInvoice() " + e.StackTraceAsString)
		}
	}

  /**
   * function updateClaimStatus()- Updates Claim Status to "Open" in CarglassDataImportStaging table for all the Claims whose invoice has been created.
   */
  private function updateClaimStatus() {
		try {
			var claims = CarglassBulkInvoiceMapper_Ext._claimStagingList
      if(claims.Count!=0){
			for (eachClaim in claims) {
				gw.transaction.Transaction.runWithNewBundle(\updateBundle -> {
					var claim = gw.api.database.Query.make(CarglassDataImportStaging).compare(CarglassDataImportStaging#CarglassReferenceNumber, Equals, eachClaim.CarglassReferenceNumber).select().first()
					updateBundle.add(claim)
					claim.Status = typekey.ClaimState.TC_OPEN    as String
				}, "su")
			}
		}
  }
	catch (e: Exception) {
					_logger.error("Exception in updateClaimStatus(). Exception is :- " + e.StackTraceAsString)
				}
	}



}