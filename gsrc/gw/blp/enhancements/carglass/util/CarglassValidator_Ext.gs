package gw.blp.enhancements.carglass.util

uses gw.blp.enhancements.carglass.beanioreader.beans.CarglassDataBean_Ext
uses gw.blp.enhancements.carglass.acordmapper.CarglassBulkInvoiceMapper_Ext
uses gw.blp.enhancements.carglass.acordmapper.CarglassClaimMapper_Ext
uses gw.pl.persistence.core.Bundle
uses gw.plugin.contact.ab800.ABContactSystemPlugin
uses gw.plugin.contact.search.ContactSearchFilter
uses gw.webservice.cc.cc801.claim.ClaimAPI
uses gw.webservice.cc.cc801.financials.bulkpay.BulkInvoiceAPI
uses org.beanio.StreamFactory
uses org.slf4j.LoggerFactory

uses java.io.File
uses java.lang.Exception
uses java.lang.StringBuilder
uses java.util.ArrayList
uses java.util.Date
uses java.util.HashMap
uses java.util.Map
uses java.text.SimpleDateFormat
uses gw.blp.enhancements.carglass.constants.CarglassConstants_Ext
/**
 * Created with IntelliJ IDEA.
 * User: Srishti.Malik1
 * Date: 26/5/16
 * Time: 10:29 AM
 * To change this template use File | Settings | File Templates.
 */
class CarglassValidator_Ext {
  private static var _logger = LoggerFactory.getLogger("carglassDailyFileLog")

  /**
   * function CGReferenceExist()- Check whether carglass reference is already processed or not. If already processed then do not create a new claim.
   * @Param VIN  Vehicle Identification Number
   * @Param lossDate Loss date
   * @Param CGReferenceNumber Unique identifier for each claim data sent in source file.
   * c
   * @Return CarglassDataImportStaging entity object
   */
  public static function CGReferenceExist(VIN: String, lossDate: Date, CGReferenceNumber: String, fileName :String): CarglassDataImportStaging {
    var query = gw.api.database.Query.make(CarglassDataImportStaging)

    try {
      query.and(\andCondition -> {
        andCondition.compare(CarglassDataImportStaging#VIN, Equals, VIN)
        andCondition.compare(CarglassDataImportStaging#LossDate, Equals, lossDate)
        andCondition.compare(CarglassDataImportStaging#CarglassReferenceNumber, Equals, CGReferenceNumber)
        andCondition.compare(CarglassDataImportStaging#FileName, Equals, fileName)
      })
    } catch (e: Exception) {
      _logger.error("Exception in populateInvoiceDataStaging() " + e.StackTraceAsString)
    }
    return query.select().AtMostOneRow
  }
  /**
   * function verifyVehicleVIN()- Check in PolicySystem whether Vehicle VIN number is valid or not. If invalid then do not create Claim.
   * @Param policyNumber  Policy Number
   * @Param policyType Policy Type
   * @Param VIN Vehicle Identification Number
   * @Return String Containing VIN validation message
   */
  public static function verifyVehicleVIN(policyNumber: String, policyType: PolicyType, VIN: String): String {
    var isValid =  CarglassConstants_Ext.INVALID_VIN
    try {
      gw.transaction.Transaction.runWithNewBundle(\newBundle -> {
        var searchCriteria = new PolicySearchCriteria()
        searchCriteria.PolicyNumber = policyNumber
        searchCriteria.PolicyType = policyType
        var retrievedPolicy = new gw.plugin.pcintegration.pc800.PolicySearchPCPlugin().searchPolicies(searchCriteria)
        if (retrievedPolicy.Summaries.Count != CarglassConstants_Ext.INT_ZERO) {
          retrievedPolicy.Summaries*.Vehicles?.each(\insuredVehicle -> {
            if (insuredVehicle.Vin == VIN){
              isValid =CarglassConstants_Ext.VALID_VIN
            }
          })
        } else {
          isValid = CarglassConstants_Ext.POLICY_NOT_FOUND
        }
      }, "su")
    } catch (e: Exception) {
    }
    return isValid
  }

  /**
   * function isFileProcessed()- Check whether the file is already processed or not. If already processed, then do not process the source file again
   * @Param fileName Name of the source file.
   * @Return CarglassDataImportStaging Entity object
   */
  public static function isFileProcessed(fileName: String): CarglassDataImportStaging {
    var query = gw.api.database.Query.make(CarglassDataImportStaging)
    try {
      query.compare(CarglassDataImportStaging#FileName, Equals, fileName)
    } catch (e: Exception) {
      _logger.error("Exception in isFileProcessed: " + e.StackTraceAsString)
    }
    return query.select().getAtMostOneRow()
  }

  /**
   * function isDataValid()- Check whether the data is valid or not. If Invalid, then do not create claim.
   * @Param record source file data object
   * @Return HashMap Containing Error messages
   */
  public static function isDataValid(record: Object): HashMap<String, Object> {
    var claimBean = record as CarglassDataBean_Ext
    var errorMessage = new ArrayList()
    var validationMap = new HashMap<String, Object>()
    validationMap.put(CarglassConstants_Ext.IS_PROCESS, true)


   if (claimBean.H2.InsuredName.equals(CarglassConstants_Ext.EMPTY_STRING)){
      _logger.error(CarglassConstants_Ext.ERROR_MESSAGES.CLAIM_NOT_CREATED_FOR_CG_REFERENCE + claimBean.H3.CarglassReference + CarglassConstants_Ext.COMMA + CarglassConstants_Ext.ERROR_MESSAGES.INSURED_NAME +  CarglassConstants_Ext.ERROR_MESSAGES.IS_MISSING)
      errorMessage.add(CarglassConstants_Ext.ERROR_MESSAGES.CLAIM_NOT_CREATED_FOR_CG_REFERENCE + claimBean.H3.CarglassReference + CarglassConstants_Ext.COMMA + CarglassConstants_Ext.ERROR_MESSAGES.INSURED_NAME +  CarglassConstants_Ext.ERROR_MESSAGES.IS_MISSING)
      validationMap.put(CarglassConstants_Ext.ERROR_MESSAGE_KEY, errorMessage)
      validationMap.put(CarglassConstants_Ext.IS_PROCESS, false)
    }
    if (claimBean.H2.InsuredAddress.equals(CarglassConstants_Ext.EMPTY_STRING)){
      _logger.error(CarglassConstants_Ext.ERROR_MESSAGES.CLAIM_NOT_CREATED_FOR_CG_REFERENCE  + claimBean.H3.CarglassReference + CarglassConstants_Ext.COMMA + CarglassConstants_Ext.ERROR_MESSAGES.INSURED_ADDRESS + CarglassConstants_Ext.ERROR_MESSAGES.IS_MISSING)
      errorMessage.add(CarglassConstants_Ext.ERROR_MESSAGES.CLAIM_NOT_CREATED_FOR_CG_REFERENCE  + claimBean.H3.CarglassReference + CarglassConstants_Ext.COMMA + CarglassConstants_Ext.ERROR_MESSAGES.INSURED_ADDRESS + CarglassConstants_Ext.ERROR_MESSAGES.IS_MISSING)
     validationMap.put(CarglassConstants_Ext.ERROR_MESSAGE_KEY, errorMessage)
      validationMap.put(CarglassConstants_Ext.IS_PROCESS, false)
    }
    if (claimBean.H2.InsuredAddressCity.equals(CarglassConstants_Ext.EMPTY_STRING)){
      _logger.error(CarglassConstants_Ext.ERROR_MESSAGES.CLAIM_NOT_CREATED_FOR_CG_REFERENCE + claimBean.H3.CarglassReference + CarglassConstants_Ext.COMMA + CarglassConstants_Ext.ERROR_MESSAGES.INSURED_ADDRESS_CITY +  CarglassConstants_Ext.ERROR_MESSAGES.IS_MISSING)
      errorMessage.add(CarglassConstants_Ext.ERROR_MESSAGES.CLAIM_NOT_CREATED_FOR_CG_REFERENCE + claimBean.H3.CarglassReference + CarglassConstants_Ext.COMMA + CarglassConstants_Ext.ERROR_MESSAGES.INSURED_ADDRESS_CITY +  CarglassConstants_Ext.ERROR_MESSAGES.IS_MISSING)
     validationMap.put(CarglassConstants_Ext.ERROR_MESSAGE_KEY, errorMessage)
      validationMap.put(CarglassConstants_Ext.IS_PROCESS, false)
    }
    if (claimBean.H3.PolicyNumber.equals(CarglassConstants_Ext.EMPTY_STRING)){
      _logger.error(CarglassConstants_Ext.ERROR_MESSAGES.CLAIM_NOT_CREATED_FOR_CG_REFERENCE + claimBean.H3.CarglassReference + CarglassConstants_Ext.COMMA + CarglassConstants_Ext.ERROR_MESSAGES.POLICY_NUMBER +  CarglassConstants_Ext.ERROR_MESSAGES.IS_MISSING)
      errorMessage.add(CarglassConstants_Ext.ERROR_MESSAGES.CLAIM_NOT_CREATED_FOR_CG_REFERENCE + claimBean.H3.CarglassReference + CarglassConstants_Ext.COMMA + CarglassConstants_Ext.ERROR_MESSAGES.POLICY_NUMBER +  CarglassConstants_Ext.ERROR_MESSAGES.IS_MISSING)
     validationMap.put(CarglassConstants_Ext.ERROR_MESSAGE_KEY, errorMessage)
      validationMap.put(CarglassConstants_Ext.IS_PROCESS, false)
    }
    if (claimBean.H3.Vin.equals(CarglassConstants_Ext.EMPTY_STRING)){
      _logger.error(CarglassConstants_Ext.ERROR_MESSAGES.CLAIM_NOT_CREATED_FOR_CG_REFERENCE  + claimBean.H3.CarglassReference + CarglassConstants_Ext.COMMA +CarglassConstants_Ext.ERROR_MESSAGES.VEHICLE_CHASIS_NUMBER+   CarglassConstants_Ext.ERROR_MESSAGES.IS_MISSING)
      errorMessage.add(CarglassConstants_Ext.ERROR_MESSAGES.CLAIM_NOT_CREATED_FOR_CG_REFERENCE  + claimBean.H3.CarglassReference + CarglassConstants_Ext.COMMA +CarglassConstants_Ext.ERROR_MESSAGES.VEHICLE_CHASIS_NUMBER+   CarglassConstants_Ext.ERROR_MESSAGES.IS_MISSING)
     validationMap.put(CarglassConstants_Ext.ERROR_MESSAGE_KEY, errorMessage)
    }

    //retrieve policy details from policy center, if VIN matches process the data else stop the process
    var policyType: PolicyType
    if (CarglassConstants_Ext.NO_N.equalsIgnoreCase(claimBean.H2.VatLiable)) policyType = typekey.PolicyType.TC_PERSONALAUTO
    else policyType = typekey.PolicyType.TC_BUSINESSAUTO

    var isVINVerified = verifyVehicleVIN(claimBean.H3.PolicyNumber, policyType, claimBean.H3.Vin)
    if (isVINVerified.equalsIgnoreCase(CarglassConstants_Ext.INVALID_VIN)){
      _logger.error(CarglassConstants_Ext.ERROR_MESSAGES.CLAIM_NOT_CREATED_FOR_CG_REFERENCE + claimBean.H3.CarglassReference + CarglassConstants_Ext.COMMA + CarglassConstants_Ext.ERROR_MESSAGES.VEHICLE_CHASIS_NUMBER + claimBean.H3.Vin +CarglassConstants_Ext.ERROR_MESSAGES.IS_INVALID )
      errorMessage.add(CarglassConstants_Ext.ERROR_MESSAGES.CLAIM_NOT_CREATED_FOR_CG_REFERENCE + claimBean.H3.CarglassReference + CarglassConstants_Ext.COMMA + CarglassConstants_Ext.ERROR_MESSAGES.VEHICLE_CHASIS_NUMBER + claimBean.H3.Vin +CarglassConstants_Ext.ERROR_MESSAGES.IS_INVALID )
     validationMap.put(CarglassConstants_Ext.ERROR_MESSAGE_KEY, errorMessage)
      validationMap.put(CarglassConstants_Ext.IS_PROCESS, false)
    }

    if (claimBean.H3.VehicleLicensePlate.equals(CarglassConstants_Ext.EMPTY_STRING)){
      _logger.error(CarglassConstants_Ext.ERROR_MESSAGES.CLAIM_NOT_CREATED_FOR_CG_REFERENCE  + claimBean.H3.CarglassReference + CarglassConstants_Ext.COMMA +CarglassConstants_Ext.ERROR_MESSAGES.VEHICLE_LICENSE_PLATE+   CarglassConstants_Ext.ERROR_MESSAGES.IS_MISSING)
      errorMessage.add(CarglassConstants_Ext.ERROR_MESSAGES.CLAIM_NOT_CREATED_FOR_CG_REFERENCE  + claimBean.H3.CarglassReference + CarglassConstants_Ext.COMMA +CarglassConstants_Ext.ERROR_MESSAGES.VEHICLE_LICENSE_PLATE+   CarglassConstants_Ext.ERROR_MESSAGES.IS_MISSING)
     validationMap.put(CarglassConstants_Ext.ERROR_MESSAGE_KEY, errorMessage)
      validationMap.put(CarglassConstants_Ext.IS_PROCESS, false)
    }
    if (claimBean.H3.LossDate == null){
      _logger.error(CarglassConstants_Ext.ERROR_MESSAGES.CLAIM_NOT_CREATED_FOR_CG_REFERENCE  + claimBean.H3.CarglassReference + CarglassConstants_Ext.COMMA + CarglassConstants_Ext.ERROR_MESSAGES.DATE_OF_LOSS+   CarglassConstants_Ext.ERROR_MESSAGES.IS_MISSING)
      errorMessage.add(CarglassConstants_Ext.ERROR_MESSAGES.CLAIM_NOT_CREATED_FOR_CG_REFERENCE  + claimBean.H3.CarglassReference + CarglassConstants_Ext.COMMA + CarglassConstants_Ext.ERROR_MESSAGES.DATE_OF_LOSS+   CarglassConstants_Ext.ERROR_MESSAGES.IS_MISSING)
     validationMap.put(CarglassConstants_Ext.ERROR_MESSAGE_KEY, errorMessage)
      validationMap.put(CarglassConstants_Ext.IS_PROCESS, false)
    }
    if (claimBean.H3.VehicleMake.equals(CarglassConstants_Ext.EMPTY_STRING)){
      _logger.error(CarglassConstants_Ext.ERROR_MESSAGES.CLAIM_NOT_CREATED_FOR_CG_REFERENCE   + claimBean.H3.CarglassReference + CarglassConstants_Ext.COMMA + CarglassConstants_Ext.ERROR_MESSAGES.VEHICLE_MAKE+  CarglassConstants_Ext.ERROR_MESSAGES.IS_MISSING)
      errorMessage.add(CarglassConstants_Ext.ERROR_MESSAGES.CLAIM_NOT_CREATED_FOR_CG_REFERENCE   + claimBean.H3.CarglassReference + CarglassConstants_Ext.COMMA + CarglassConstants_Ext.ERROR_MESSAGES.VEHICLE_MAKE+  CarglassConstants_Ext.ERROR_MESSAGES.IS_MISSING)
     validationMap.put(CarglassConstants_Ext.ERROR_MESSAGE_KEY, errorMessage)
      validationMap.put(CarglassConstants_Ext.IS_PROCESS, false)
    }
    if (claimBean.H3.VehicleModel.equals(CarglassConstants_Ext.EMPTY_STRING)){
      _logger.error(CarglassConstants_Ext.ERROR_MESSAGES.CLAIM_NOT_CREATED_FOR_CG_REFERENCE   + claimBean.H3.CarglassReference + CarglassConstants_Ext.COMMA + CarglassConstants_Ext.ERROR_MESSAGES.VEHICLE_MODEL+  CarglassConstants_Ext.ERROR_MESSAGES.IS_MISSING)
      errorMessage.add(CarglassConstants_Ext.ERROR_MESSAGES.CLAIM_NOT_CREATED_FOR_CG_REFERENCE   + claimBean.H3.CarglassReference + CarglassConstants_Ext.COMMA + CarglassConstants_Ext.ERROR_MESSAGES.VEHICLE_MODEL+  CarglassConstants_Ext.ERROR_MESSAGES.IS_MISSING)
     validationMap.put(CarglassConstants_Ext.ERROR_MESSAGE_KEY, errorMessage)
      validationMap.put(CarglassConstants_Ext.IS_PROCESS, false)
    }
    if (claimBean.H3.VehicleYear.equals(CarglassConstants_Ext.EMPTY_STRING)){
      _logger.error(CarglassConstants_Ext.ERROR_MESSAGES.CLAIM_NOT_CREATED_FOR_CG_REFERENCE  + claimBean.H3.CarglassReference + CarglassConstants_Ext.COMMA + CarglassConstants_Ext.ERROR_MESSAGES.VEHICLE_YEAR_BUILT+  CarglassConstants_Ext.ERROR_MESSAGES.IS_MISSING)
      errorMessage.add(CarglassConstants_Ext.ERROR_MESSAGES.CLAIM_NOT_CREATED_FOR_CG_REFERENCE  + claimBean.H3.CarglassReference + CarglassConstants_Ext.COMMA + CarglassConstants_Ext.ERROR_MESSAGES.VEHICLE_YEAR_BUILT+  CarglassConstants_Ext.ERROR_MESSAGES.IS_MISSING)
     validationMap.put(CarglassConstants_Ext.ERROR_MESSAGE_KEY, errorMessage)
      validationMap.put(CarglassConstants_Ext.IS_PROCESS, false)
    }
    if (claimBean.H0.RepairShopNumber == ""){
      _logger.warn(CarglassConstants_Ext.ERROR_MESSAGES.WARNING_FOR_CG_REFERENCE + claimBean.H3.CarglassReference + CarglassConstants_Ext.COMMA +   CarglassConstants_Ext.ERROR_MESSAGES.REPAIR_SHOP_NUMBER+ CarglassConstants_Ext.ERROR_MESSAGES.IS_MISSING)
      errorMessage.add(CarglassConstants_Ext.ERROR_MESSAGES.WARNING_FOR_CG_REFERENCE + claimBean.H3.CarglassReference + CarglassConstants_Ext.COMMA +   CarglassConstants_Ext.ERROR_MESSAGES.REPAIR_SHOP_NUMBER+ CarglassConstants_Ext.ERROR_MESSAGES.IS_MISSING)
     validationMap.put(CarglassConstants_Ext.ERROR_MESSAGE_KEY, errorMessage)
    }
    return validationMap
  }
}