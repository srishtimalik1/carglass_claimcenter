package gw.blp.enhancements.carglass.acordmapper

uses gw.blp.enhancements.carglass.beanioreader.beans.CarglassDataBean_Ext

uses java.lang.Exception
uses java.lang.Integer
uses java.text.SimpleDateFormat
uses java.util.ArrayList
uses java.util.Date
uses java.util.HashMap
uses gw.blp.enhancements.carglass.constants.CarglassConstants_Ext
/**
 * CarglassClaimMapper_Ext prepares an acordMXL object from the data received from Carglass.
 * Main class for mapping an ACORD XML document to an FNOL Claim.
 * Author: Srishti.Malik1
 */
class CarglassClaimMapper_Ext {
  private var acordXml: xsd.acord.ACORD_Type
  construct(cgData: CarglassDataBean_Ext, contactTaxID: String) {
    acordXml = new xsd.acord.ACORD_Type()
    var acordRequest = new xsd.acord.ACORD_Type_ACORDREQ()
    var claimSvcReq = new xsd.acord.ClaimsSvcRq_Type()
    var claimsSvReqMsgs = new xsd.acord.ClaimsSvcRq_Type_CLAIMSSVCRQMSGS()
    var claimsSvReqMsgs_choice = new xsd.acord.ClaimsSvcRq_Type_CLAIMSSVCRQMSGS_Choice()
    var claimsNotificationAddReq = new xsd.acord.ClaimsNotificationAddRq_Type()
    var claimsNotificationAddReq_choice = new xsd.acord.ClaimsNotificationAddRq_Type_Choice()

    claimsNotificationAddReq.ClaimsOccurrence = populateClaimDetails(cgData)
    claimsNotificationAddReq.ClaimsPartys = populateClaimParties(cgData)
    claimsNotificationAddReq_choice.AutoLossInfos = populateIncidentsAndExposures(cgData, claimsNotificationAddReq.ClaimsPartys)

    claimsNotificationAddReq.Policy = populatePolicyDetails(cgData)
    var carglassMap = new HashMap<String, String>()
    carglassMap.put(CarglassConstants_Ext.FNOL_CALLER, CarglassConstants_Ext.CARGLASS)
    carglassMap.put(CarglassConstants_Ext.CONTACT_TAX_ID, contactTaxID)
    carglassMap.put(CarglassConstants_Ext.DEBTOR_BALANCE, cgData.H5.get(CarglassConstants_Ext.DEBTOR_BALANCE).toString().trim())
    claimsNotificationAddReq.setAttributes(carglassMap)
    claimsNotificationAddReq.RqUID = cgData.H0.CorrelationID

    claimsNotificationAddReq.Choice = claimsNotificationAddReq_choice


    claimsNotificationAddReq.ClaimsPartys = populateClaimParties(cgData)
    claimsNotificationAddReq_choice.AutoLossInfos = populateIncidentsAndExposures(cgData, claimsNotificationAddReq.ClaimsPartys)
    claimsNotificationAddReq.CurCd = typekey.Currency.TC_USD as String
    claimsSvReqMsgs_choice.ClaimsNotificationAddRq = claimsNotificationAddReq
    claimsSvReqMsgs.Choice = claimsSvReqMsgs_choice
    claimSvcReq.CLAIMSSVCRQMSGSs.add(claimsSvReqMsgs)
    acordRequest.ClaimsSvcRqs.add(claimSvcReq)

    acordXml.ACORDREQ = acordRequest
  }
  /**
   * function populatePolicyDetails()- maps policy details in the acord policy object
   * @Param CarglassDataBean_Ext carglass data
   * @return xsd.acord.PCPOLICY acord policy object
   */
  private function populatePolicyDetails(cgData: CarglassDataBean_Ext): xsd.acord.PCPOLICY {
    var policyRq = new xsd.acord.PCPOLICY()
    var lobRq = new xsd.acord.LineOfBusiness()
    var lobEnum: xsd.acord.enums.LineOfBusinessEnum
    if (CarglassConstants_Ext.NO_N.equalsIgnoreCase(cgData.H2.VatLiable)) {
      lobEnum = xsd.acord.enums.LineOfBusinessEnum.AUTOP
    } else {
      lobEnum = xsd.acord.enums.LineOfBusinessEnum.AUTO
    }
    lobRq.Value = lobEnum
    policyRq.PolicyNumber = cgData.H3.PolicyNumber
    policyRq.LOBCd = lobEnum
    policyRq.LOBCd_elem = lobRq
    var expirationDate = new xsd.acord.Date()
    var effectiveDate = new xsd.acord.Date()

    effectiveDate.Value = new Date("01/01/${cgData.H3.InvoiceReportedDate.YearOfDate}") as String
    expirationDate.Value = new Date("12/31/${cgData.H3.InvoiceReportedDate.YearOfDate}") as String
    policyRq.ContractTerm.EffectiveDt_elem = effectiveDate
    policyRq.ContractTerm.ExpirationDt_elem = expirationDate
    return policyRq
  }
  /**
   * function populateClaimDetails()- maps claim details in the acord claim object
   * @Param CarglassDataBean_Ext carglass data
   * @return xsd.acord.ClaimsOccurrence_Type acord claim object
   */
  private function populateClaimDetails(cgData: CarglassDataBean_Ext): xsd.acord.ClaimsOccurrence_Type {
    var claimsOccurrence = new xsd.acord.ClaimsOccurrence_Type()
    try {
      var claimsReported = new xsd.acord.ClaimsReported_Type()

      //Loss Date
      var lDate = new SimpleDateFormat("yyyy-MM-dd").format(cgData.H3.LossDate)
      var lossDate = new xsd.acord.Date()
      lossDate.Value = lDate
      claimsOccurrence.LossDt_elem = lossDate
      //date of notice
      var repDate = new xsd.acord.DateTime()
      if (null != cgData.H3.InvoiceReportedDate) {
        var rDate = new SimpleDateFormat("yyyy-MM-dd").format(cgData.H3.InvoiceReportedDate)
        repDate.Value = rDate
      } else {
        repDate.Value = lDate
        //If date of notice is empty, use loss date
      }
      claimsReported.ReportedDt_elem = repDate
      claimsOccurrence.ClaimsReporteds.add(claimsReported)
      var address = populateInsuredAddress(cgData)
      claimsOccurrence.Addr = address.get(CarglassConstants_Ext.INT_ZERO)
      // TODO check
      //loss description
      claimsOccurrence.IncidentDesc = CarglassConstants_Ext.INCIDENT_DESCRIPTION + CarglassConstants_Ext.LOSS_CAUSE + cgData.H3.DamageCauseCode + " " + cgData.H3.DamageCauseDescription.trim()
    } catch (e: Exception) {
      print(" function populateClaimDetails")
      print(e.StackTraceAsString)
    }
    return claimsOccurrence
  }
  /**
   * function populateIncidentsAndExposures()- maps Incidents And Exposures details in the acord object
   * @Param CarglassDataBean_Ext carglass data
   * @Param claimPartyList contains list of contacts involved in the claim
   * @return xsd.acord.AutoLossInfo_Type acord Auto Loss object
   */
  private function populateIncidentsAndExposures(cgData: CarglassDataBean_Ext, claimPartyList: List<xsd.acord.ClaimsParty_Type>): ArrayList<xsd.acord.AutoLossInfo_Type> {
    var autoLossInfoList = new ArrayList<xsd.acord.AutoLossInfo_Type>()
    var autoLossInfo = new xsd.acord.AutoLossInfo_Type()
    try {
      autoLossInfo.VehInfo.Manufacturer = cgData.H3.VehicleMake
      autoLossInfo.VehInfo.Model = cgData.H3.VehicleModel + " " + cgData.H3.VehicleType
      autoLossInfo.VehInfo.ModelYear = cgData.H3.VehicleYear as Integer
      autoLossInfo.VehInfo.VehIdentificationNumber = cgData.H3.Vin
      autoLossInfo.VehInfo.LicensePlateNumber = cgData.H3.VehicleLicensePlate
      autoLossInfo.DamageDesc = CarglassConstants_Ext.LOSS_CAUSE + cgData.H3.DamageCauseCode + " " + cgData.H3.DamageCauseDescription.trim() + CarglassConstants_Ext.VEHICLE_MILEAGE + cgData.H3.VehicleMileage.trim()
      autoLossInfo.ClaimsPartyRefs = claimPartyList
      autoLossInfoList.add(autoLossInfo)
    } catch (e: Exception) {
      print(" function autoLossInfoList")
      print(e.StackTraceAsString)
    }
    return autoLossInfoList
  }
  /**
   * function populateClaimParties()- maps claim parties involved details in the acord claim party object
   * @Param CarglassDataBean_Ext carglass data
   * @return xsd.acord.ClaimsParty_Type acord Claims Party object containing claim parties details
   */
  private function populateClaimParties(cgData: CarglassDataBean_Ext): ArrayList<xsd.acord.ClaimsParty_Type> {
    var claimPartyTypeList = new ArrayList<xsd.acord.ClaimsParty_Type>()
    var claimPartyType = new xsd.acord.ClaimsParty_Type()
    try {
      var nameInfoList = new ArrayList<xsd.acord.NameInfo_Type>()
      var nameInfo = new xsd.acord.NameInfo_Type()
      var commlNameType = new xsd.acord.CommlName_Type()
      commlNameType.CommercialName = cgData.H0.RepairShopNumber + cgData.H0.RepairShopName
      nameInfo.Choice.CommlName = commlNameType

      //if contact is of type Company, then create a new person "Employee" with roles "driver, main contact, reporter, claimant"
      if (CarglassConstants_Ext.YES_Y.equalsIgnoreCase(cgData.H2.VatLiable)){
        var employeeType = new xsd.acord.ClaimsParty_Type()
        employeeType.id = CarglassConstants_Ext.CARGLASS_REFERENCE
        var employeeNameInfoList = new ArrayList<xsd.acord.NameInfo_Type>()
        var employeeNameInfo = new xsd.acord.NameInfo_Type()
        var personNameType = new xsd.acord.PersonName_Type()
        personNameType.GivenName = CarglassConstants_Ext.TECHNICAL
        personNameType.Surname = CarglassConstants_Ext.EMPLOYEE
        employeeNameInfo.Choice.PersonName = personNameType
        employeeNameInfoList.add(employeeNameInfo)

        var employeeRolesInfoType = new xsd.acord.ClaimsPartyInfo_Type()
        var employeeRoleDRV = new xsd.acord.ClaimsPartyRole()
        employeeRoleDRV.Value = xsd.acord.enums.ClaimsPartyRoleEnum.DRV
        var employeeRoleCLM = new xsd.acord.ClaimsPartyRole()
        employeeRoleCLM.Value = xsd.acord.enums.ClaimsPartyRoleEnum.CLM
        var employeeRoleReptBy = new xsd.acord.ClaimsPartyRole()
        employeeRoleReptBy.Value = xsd.acord.enums.ClaimsPartyRoleEnum.ReptBy
        var employeeRoleMCO = new xsd.acord.ClaimsPartyRole()
        employeeRoleMCO.Value = xsd.acord.enums.ClaimsPartyRoleEnum.MCO

        var employeeRole = new ArrayList<xsd.acord.ClaimsPartyRole>()
        employeeRole.add(employeeRoleDRV)
        employeeRole.add(employeeRoleCLM)
        employeeRole.add(employeeRoleReptBy)
        employeeRole.add(employeeRoleMCO)

        employeeRolesInfoType.ClaimsPartyRoleCds.addAll(employeeRole)
        employeeRolesInfoType.RelationshipToInsuredCd = xsd.acord.enums.DriverRelatesToEnum.EM
        employeeType.GeneralPartyInfo.NameInfos = employeeNameInfoList
        employeeType.GeneralPartyInfo.Addrs = populateInsuredAddress(cgData)
        employeeType.ClaimsPartyInfo = employeeRolesInfoType
        claimPartyTypeList.add(employeeType)
      }

      nameInfoList.add(nameInfo)
      claimPartyType.GeneralPartyInfo.NameInfos = nameInfoList
      claimPartyType.GeneralPartyInfo.Addrs = populateRepairShopAddress(cgData)

      //TODO - set below

      var claimsPartyInfoType = new xsd.acord.ClaimsPartyInfo_Type()
      var claimsPartyRole = new xsd.acord.ClaimsPartyRole()
      claimsPartyRole.Value = xsd.acord.enums.ClaimsPartyRoleEnum.RepShop
      claimsPartyInfoType.ClaimsPartyRoleCds.add(claimsPartyRole)
      claimPartyType.ClaimsPartyInfo = claimsPartyInfoType
      claimPartyTypeList.add(claimPartyType)

      //populate insured details (person or company)
      var insuredType = populateInsured(cgData)
      claimPartyTypeList.add(insuredType)
    } catch (e: Exception) {
      print(" function claimPartyTypeList")
      print(e.StackTraceAsString)
    }

    return claimPartyTypeList
  }

  /**
   * function populateGeneralPartyInfo()- maps claim parties involved details in the acord GeneralPart object
   * @Param CarglassDataBean_Ext carglass data
   * @return xsd.acord.GeneralPartyInfo_Type acord General Party object containing claim parties details
   */
  private function populateGeneralPartyInfo(cgData: CarglassDataBean_Ext): xsd.acord.GeneralPartyInfo_Type {
    var generalPartyInfo = new xsd.acord.GeneralPartyInfo_Type()
    try {
      var nameInfoList = new ArrayList<xsd.acord.NameInfo_Type>()
      var nameInfo = new xsd.acord.NameInfo_Type()
      if (CarglassConstants_Ext.NO_N.equalsIgnoreCase(cgData.H2.VatLiable)) {
        // if contact is of type "Person"
        if (cgData.H2.InsuredName.contains(CarglassConstants_Ext.COMMA)) {
          var personName = cgData.H2.InsuredName.split(CarglassConstants_Ext.COMMA)
          nameInfo.Choice.PersonName.Surname = personName[CarglassConstants_Ext.INT_ZERO]
          if(null!= personName[CarglassConstants_Ext.INT_ONE])   nameInfo.Choice.PersonName.GivenName = personName[CarglassConstants_Ext.INT_ONE]
         else if(null!= personName[CarglassConstants_Ext.INT_TWO]) nameInfo.Choice.PersonName.GivenName = personName[CarglassConstants_Ext.INT_TWO]
        } else {
          nameInfo.Choice.PersonName.Surname = cgData.H2.InsuredName
          nameInfo.Choice.PersonName.GivenName = cgData.H2.InsuredName
        }
      } else if (CarglassConstants_Ext.YES_Y.equalsIgnoreCase(cgData.H2.VatLiable)){
        // if contact is of type "Company"
        var commlNameType = new xsd.acord.CommlName_Type()
        commlNameType.CommercialName = cgData.H2.InsuredName
        nameInfo.Choice.CommlName = commlNameType
      }
      nameInfoList.add(nameInfo)
      generalPartyInfo.NameInfos = nameInfoList
      generalPartyInfo.Addrs = populateInsuredAddress(cgData)
      generalPartyInfo.Communications = populateInsuredCommunications(cgData)
    } catch (e: Exception) {
      print(e.StackTraceAsString)
      print("function generalPartyInfo")
    }
    return generalPartyInfo
  }

  /**
   * function populateInsuredCommunications()- maps communication details in the acord communication type object
   * @Param CarglassDataBean_Ext carglass data
   * @return xsd.acord.Communications_Type acord Communications Type object containing claim parties communication details
   */
  private function populateInsuredCommunications(cgData: CarglassDataBean_Ext): xsd.acord.Communications_Type {
    var communications = new xsd.acord.Communications_Type()
    try {
      var phoneInfo = new xsd.acord.PhoneInfo_Type()
      phoneInfo.PhoneNumber = cgData.H2.InsuredTelephone
      phoneInfo.PhoneTypeCd = xsd.acord.enums.PhoneTypeEnum.Cell
      communications.PhoneInfos.add(phoneInfo)
    } catch (e: Exception) {
      print(e.StackTraceAsString)
      print("function communications")
    }
    return communications
  }

  /**
   * function populateInsured()- maps Insured details in the acord claim party type object
   * @Param CarglassDataBean_Ext carglass data
   * @return xsd.acord.ClaimsParty_Type acord Claims Party Type object containing insured details
   */
  private function populateInsured(cgData: CarglassDataBean_Ext): xsd.acord.ClaimsParty_Type {
    var insuredInfoType = new xsd.acord.ClaimsPartyInfo_Type()
    var insuredType = new xsd.acord.ClaimsParty_Type()
    var insuredRoleTypeList = new ArrayList<xsd.acord.ClaimsPartyRole>()
    var insuredRole1 = new xsd.acord.ClaimsPartyRole()
    try {

      insuredType.id = CarglassConstants_Ext.CARGLASS_REFERENCE
      //If insured= Person or Company, Role- insured
     // insuredRole1.setText("Insured")
      insuredRole1.Value = xsd.acord.enums.ClaimsPartyRoleEnum.Insured

      //If insured= Person, add Roles- Claimant, Driver, Reporter, Main Contact
      if (CarglassConstants_Ext.NO_N.equalsIgnoreCase(cgData.H2.VatLiable)) {
        var insuredRole2 = new xsd.acord.ClaimsPartyRole()
        var insuredRole3 = new xsd.acord.ClaimsPartyRole()
        var insuredRole4 = new xsd.acord.ClaimsPartyRole()
        var insuredRole5 = new xsd.acord.ClaimsPartyRole()

       // insuredRole2.setText("CLM")
        insuredRole2.Value = xsd.acord.enums.ClaimsPartyRoleEnum.CLM
        //no condition for this (DRV) role in acord contact acordmapper            TODO
        //insuredRole3.setText("DRV")
        insuredRole3.Value = xsd.acord.enums.ClaimsPartyRoleEnum.DRV
        //Driver
        //insuredRole4.setText("ReptBy")
        insuredRole4.Value = xsd.acord.enums.ClaimsPartyRoleEnum.ReptBy
        //Reporter
        //insuredRole5.setText("MainContact")
        insuredRole5.Value = xsd.acord.enums.ClaimsPartyRoleEnum.MCO
        //TODO Check
        insuredRoleTypeList.add(insuredRole2)
        insuredRoleTypeList.add(insuredRole3)
        insuredRoleTypeList.add(insuredRole4)
        insuredRoleTypeList.add(insuredRole5)
      }

      insuredRoleTypeList.add(insuredRole1)

      insuredInfoType.ClaimsPartyRoleCds.addAll(insuredRoleTypeList)
      insuredInfoType.RelationshipToInsuredCd = xsd.acord.enums.DriverRelatesToEnum.IN
      insuredType.GeneralPartyInfo = populateGeneralPartyInfo(cgData)
      insuredType.GeneralPartyInfo.Addrs = populateInsuredAddress(cgData)
      insuredType.ClaimsPartyInfo = insuredInfoType
    } catch (e: Exception) {
      print(e.StackTraceAsString)
      print("function populatePrincipals")
    }
    return insuredType
  }
  /**
   * function populateInsuredAddress()- maps Insured address details in the acord Address Type object
   * @Param CarglassDataBean_Ext carglass data
   * @return xsd.acord.Addr_Type acord Addr_Type object containing insured address details
   */
  private function populateInsuredAddress(cgData: CarglassDataBean_Ext): List<xsd.acord.Addr_Type> {
    var addressList = new ArrayList<xsd.acord.Addr_Type>()
    try {
      var address = new xsd.acord.Addr_Type()
      address.City = cgData.H2.InsuredAddressCity
      address.PostalCode = cgData.H2.InsuredPostalCode
      address.Choice.Addr1 = cgData.H2.InsuredAddress
      address.Country = typekey.Country.TC_BE     as String
      addressList.add(address)
    } catch (e: Exception) {
      print(e.StackTraceAsString)
      print("function populateInsuredAddress")
    }
    return addressList
  }
  /**
   * function populateRepairShopAddress()- maps repair shop address details in the acord Address Type object
   * @Param CarglassDataBean_Ext carglass data
   * @return xsd.acord.Addr_Type acord Addr_Type object containing repair shop address details
   */
  private function populateRepairShopAddress(cgData: CarglassDataBean_Ext): List<xsd.acord.Addr_Type> {
    var addressList = new ArrayList<xsd.acord.Addr_Type>()
    try {
      var address = new xsd.acord.Addr_Type()
      address.City = cgData.H0.RepairShopAddressCity
      address.PostalCode = cgData.H0.RepairShopAddressPostalCode
      address.Choice.Addr1 = cgData.H0.RepairShopAddress1 + cgData.H0.RepairShopAddress2
      address.Country = typekey.Country.TC_BE    as String
      addressList.add(address)
    } catch (e: Exception) {
      print(e.StackTraceAsString)
      print("function populateRepairShopAddress")
    }
    return addressList
  }

  public property get AcordXML(): String {
    return acordXml.asUTFString()
  }
}