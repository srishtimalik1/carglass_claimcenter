package gw.blp.enhancements.carglass.acordmapper

uses gw.plugin.contact.ab800.ABContactSystemPlugin
uses gw.plugin.contact.search.ContactSearchFilter
uses gw.webservice.cc.cc801.dto.BulkInvoiceDTO
uses gw.webservice.cc.cc801.dto.BulkInvoiceItemDTO
uses gw.webservice.cc.cc801.dto.BulkInvoiceItemInfoDTO

uses java.lang.Exception
uses java.math.BigDecimal
uses java.util.ArrayList
uses java.lang.Long

/**
 * CarglassBulkInvoiceMapper_Ext prepares the Bulk Invoice DTO which is used to create the bulk invoice for all the claims in each file sent by the Carglass
 * Author: Srishti.Malik1
 *
 */
class CarglassBulkInvoiceMapper_Ext {
  public static var _claimStagingList: List<CarglassDataImportStaging>
  /**
   * function prepareBulkInvoiceDTO()- Prepared BulkInvoiceDTO for all the claims sent in flat file having status as Draft in carglass staging table.
   * Fetches the Check Payee Details  from Contact Manager, with search criteria as Contact's Tax ID
   * @Param taxID Contact's Tax ID to be used to serach contact from contact manager
   * @Param fileName Source file name which is in process
   * @return   BulkInvoiceDTO having details of all the claims which are in draft status
   */
  public function prepareBulkInvoiceDTO(taxID: String, fileName: String): BulkInvoiceDTO {
    var invoiceDTO: BulkInvoiceDTO
    try {
      var abContact: Contact

      var invoiceItemDTO = new BulkInvoiceItemDTO()
      var newItemsArray = new ArrayList<BulkInvoiceItemDTO>()
      _claimStagingList = new ArrayList<CarglassDataImportStaging>()
      var payeeID: String
      var claimStagingData = gw.api.database.Query.make(CarglassDataImportStaging)
          .and(\andCondition -> {
            andCondition.compare(CarglassDataImportStaging#FileName, Equals, fileName)
            andCondition.compare(CarglassDataImportStaging#Status, Equals, typekey.ClaimState.TC_DRAFT as String)
            andCondition.compare(CarglassDataImportStaging#ClaimNumber, NotEquals, null)
          }).select()


  if (claimStagingData.HasElements){
        _claimStagingList = claimStagingData.toList()
        invoiceDTO = new BulkInvoiceDTO()
        gw.transaction.Transaction.runWithNewBundle(\bundle -> {
          var searchCriteria = new ContactSearchCriteria()
          searchCriteria.TaxID = taxID
          searchCriteria.ContactSubtype = typekey.Contact.TC_COMPANYVENDOR
          var contactSearchResult = new ABContactSystemPlugin().searchContacts(searchCriteria, new ContactSearchFilter())
          abContact = contactSearchResult.getContacts().first()
          payeeID = abContact.PublicID
        }, "su")
        if (null != abContact.PublicID) {
          invoiceDTO.PayeeID = abContact.PublicID
        } else {
          invoiceDTO.PayeeID = payeeID
        }

        foreach (eachClaim in claimStagingData) {
          validateReserveAmount(eachClaim)
          invoiceItemDTO = prepareBulkInvoiceItemDTO(eachClaim)
          newItemsArray.add(invoiceItemDTO)
        }


        invoiceDTO.NewInvoiceItems = newItemsArray
      }
    } catch (e: Exception) {
      print(e.StackTraceAsString)
    }
    return invoiceDTO
  }

  /**
   * function prepareBulkInvoiceItemDTO()- Prepared BulkInvoiceItemDTO for each claim
   * @Param CarglassDataImportStaging each claim whopse invoice is to be created
   * @return   BulkInvoiceItemDTO having details of each claim whose invoice is to be created
   */
  public function prepareBulkInvoiceItemDTO(eachData: CarglassDataImportStaging): BulkInvoiceItemDTO {
    var invoiceItemDTO = new BulkInvoiceItemDTO()
    invoiceItemDTO.ClaimID = eachData.ClaimNumber.PublicID
    var totalAmount = eachData.CarglassInvoices.sum(\elt -> elt.Amount as BigDecimal).toBigInteger() as gw.api.financials.CurrencyAmount
    invoiceItemDTO.Amount = eachData.CarglassInvoices.sum(\elt -> elt.Amount as BigDecimal).toBigInteger() as gw.api.financials.CurrencyAmount
    invoiceItemDTO.CostCategory = CostCategory.TC_AUTOGLASS
    invoiceItemDTO.Status = BulkInvoiceItemStatus.TC_DRAFT
    invoiceItemDTO.ReservingCurrency = typekey.Currency.TC_EUR
    invoiceItemDTO.CostType = CostType.TC_AOEXPENSE
    invoiceItemDTO.PaymentType = PaymentType.TC_FINAL
    invoiceItemDTO.ExposureID = eachData.ClaimNumber.Exposures.first().PublicID
    invoiceItemDTO.NonEroding = false
    return invoiceItemDTO
  }

  /**
   * function validateReserveAmount()- validate the reserve amount of a claim. If the current amount is not equivalent to the total amount then a new Reserve will be created of total amount.
   * @Param CarglassDataImportStaging each claim whopse invoice is to be created
   */
  private function validateReserveAmount(eachData: CarglassDataImportStaging) {
    var total = eachData.CarglassInvoices.sum(\elt -> elt.Amount as BigDecimal).toBigInteger() as gw.api.financials.CurrencyAmount
    var amount = eachData.ClaimNumber.Exposures.first().OpenReserves
    if (total != amount){
      gw.transaction.Transaction.runWithNewBundle(\bundle -> {
        var su = gw.api.database.Query.make(entity.User).compare(User#PublicID, Equals, "default_data:1").select().first()
        //TODO -     get user entity using Util getCurrent User
        var exp = eachData.ClaimNumber.Exposures.first()
        var initialReserve = bundle.add(exp.ReserveLines.first())
        bundle.add(initialReserve)
        initialReserve.setAvailableReserves(total as BigDecimal, su)
      }, "su")
    }
  }
}