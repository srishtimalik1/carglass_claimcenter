package gw.blp.enhancements.carglass.constants

/**
 * CarglassConstants_Ext interface defines constants required for Carglass Accelerator
 * Author: Srishti.Malik1
 */
public interface CarglassConstants_Ext {
 public static final var RECORD_STREAM: String =   "recordStream"
   public static final var RECORD: String =   "record"
  public static final var HQ_TAX_ID: String = "headquartersTaxID"
  public static final var ERROR_MESSAGE_KEY: String = "errorMessage"
   public static final var IS_PROCESS: String =  "isProcess"
  public static final var DEBTOR_BALANCE: String = "debtorBalance"
  public static final var INVALID_POLICY: String = "InvalidPolicy"
  public static final var CONTACT_TAX_ID: String = "contactTaxID"
  public static final var FNOL_CALLER: String = "FNOLcaller"
  public static final var CARGLASS: String = "Carglass"
 public static final var INCIDENT_DESCRIPTION: String = "Repair of Windshield or other parts by Carglass. "
  public static final var LOSS_CAUSE: String = "Loss Cause: "
  public static final var VEHICLE_MILEAGE: String = "Vehicle Mileage in km_"
  public static final var TECHNICAL: String = "Technical"
  public static final var EMPLOYEE: String = "Employee"
  public static final var CARGLASS_REFERENCE: String = "CarglassReference"
   public static final var INVALID_VIN: String = "InvalidVIN"
   public static final var VALID_VIN: String = "ValidVIN"
 public static final var POLICY_NOT_FOUND: String = "PolicyNotfound"
  public enum ERROR_MESSAGES{
    public static final var CLAIM_NOT_CREATED_FOR_CG_REFERENCE: String = "Claim Not Created for Carglass reference number: "
    public static final var WARNING_FOR_CG_REFERENCE: String = "Warning: For Carglass reference number: "
    public static final var IS_MISSING: String = " is missing "
    public static final var IS_INVALID: String = " is invalid"
    public static final var INSURED_NAME: String = " Insured Name "
    public static final var INSURED_ADDRESS: String =  " Insured Address "
   public static final var INSURED_ADDRESS_CITY: String =   " Insured Address City"
  public static final var POLICY_NUMBER: String =   " Policy Number"
   public static final var VEHICLE_CHASIS_NUMBER: String =  " Vehicle Chasis number "
  public static final var VEHICLE_LICENSE_PLATE: String =   " Vehicle License Plate "
    public static final var DATE_OF_LOSS: String =  " Date of Loss "
    public static final var VEHICLE_MAKE: String =   " Vehicle Make "
 public static final var VEHICLE_MODEL: String = " Vehicle Model "
    public static final var VEHICLE_YEAR_BUILT: String =  " Vehicle Year built "
   public static final var REPAIR_SHOP_NUMBER: String =  " Carglass repair shop number "
  }
  public static final var YES_Y: String = "Y"
    public static final var NO_N: String = "N"
  public static final var COMMA: String = ","
  public static final var HYPHEN: String = "-"
  public static final var EMPTY_STRING: String = ""

  public static final var INT_ZERO : int = 0
     public static final var INT_ONE : int = 1
      public static final var INT_TWO : int = 2
   public static final var INT_THREE : int = 3
  public static final var INT_SIX : int = 6




}