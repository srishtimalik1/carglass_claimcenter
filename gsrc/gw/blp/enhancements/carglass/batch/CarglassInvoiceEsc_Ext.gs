package gw.blp.enhancements.carglass.batch

uses gw.processes.BatchProcessBase
uses org.slf4j.LoggerFactory
uses java.lang.Exception
uses gw.webservice.cc.MaintenanceToolsAPI

/**
 * Batch updates the Bulk Invoice status to Requesting and updates the Claim's Activities, Exposure and Claim status to Closed
 * Author: Srishti.Malik1
 */
class CarglassInvoiceEsc_Ext extends BatchProcessBase {
  private static var _logger = LoggerFactory.getLogger("carglassDailyFileLog")
  construct() {
    super(BatchProcessType.TC_CARGLASSINVOICEESC_EXT)
  }

  /**
   * function doWork()- executes Bulk Invoice Escalation batch to update bulk invoice status to Requesting state.
   * Update Claim, Exposures and Activities state to Closed state
   */
  override function doWork() {
    try {
      gw.transaction.Transaction.runWithNewBundle(\updateBundle -> {
        new MaintenanceToolsAPI().startBatchProcess("BulkInvoiceEsc")
      }, "su")

      var claims = gw.api.database.Query.make(CarglassDataImportStaging).compare(CarglassDataImportStaging#Status, Equals, typekey.ClaimState.TC_OPEN).select()
      gw.transaction.Transaction.runWithNewBundle(\updateBundle -> {
        for (claim in claims) {
          updateBundle.add(claim)
          updateBundle.add(claim.ClaimNumber)
          claim.Status = typekey.ClaimState.TC_CLOSED
          claim.ClaimNumber.Activities.each(\activity -> {
            activity.Status = typekey.ActivityStatus.TC_COMPLETE
          })
          claim.ClaimNumber.Exposures.first().close(typekey.exposureClosedOutcomeType.TC_COMPLETED, "Carglass Exposure closed")
          claim.ClaimNumber.close(typekey.ClaimClosedOutcomeType.TC_COMPLETED, "Carglass claim closed")
        }
      }, "su")
    } catch (e: Exception) {
    }
  }
}