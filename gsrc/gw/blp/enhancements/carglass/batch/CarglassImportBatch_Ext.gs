package gw.blp.enhancements.carglass.batch

uses gw.processes.BatchProcessBase
uses org.slf4j.LoggerFactory

uses java.io.File
uses java.lang.Exception
uses gw.blp.enhancements.carglass.util.Carglass
uses gw.blp.enhancements.carglass.util.CarglassUtil_Ext


/**
 * CarglassImportBatch_Ext Batch imports Claim data into ClaimCenter from a fixed length flat file received from Carglass.
 * Author: Srishti.Malik1
 */
class CarglassImportBatch_Ext extends BatchProcessBase {
  private static var _logger = LoggerFactory.getLogger("carglassDailyFileLog")
  construct() {
    super(BatchProcessType.TC_CARGLASSIMPORT_EXT)
  }

 /**
 * function doWork()- monitors the input folder and picks all the files. For each picked file, importfile() is processed.
*/
  override function doWork() {
    try {
      var inputFilePath = Carglass.getValueByName("carglass.file.path.inputFolder")
      var inputFolder = new File(inputFilePath)
      _logger.info("CarglassImportBatch_Ext : Picking files from ${inputFilePath}")
      if (inputFolder.exists()) {
        var fileList = inputFolder.listFiles()
        for (var eachFile in fileList) {
          try {
            _logger.info("CarglassImportBatch_Ext : Processing starts for file ${eachFile}")
            new CarglassUtil_Ext().importFile(eachFile)
          } catch (e: Exception) {
            _logger.error("CarglassImportBatch_Ext : Error reading file ${eachFile} - ${e.StackTraceAsString}")
          }
        }
      } else {
        _logger.error("CarglassImportBatch_Ext : ${inputFilePath} is not accessible")
      }
    } catch (e: Exception) {
      _logger.error("CarglassImportBatch_Ext : ${e.StackTraceAsString}")
    }
  }
}